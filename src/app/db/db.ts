export const DB = {
	users: [
		{
			id: 'xasfgassxasrea',
			firstName: 'Dmytro',
			lastName: 'Domaretskyi', 
			email: 'dimanquedev@gmail.com',
			password: '0000',
			startingWeight: 75,
			logo: 'assets/img/user-logo.png',
			unitSystem: '',
			height: 165,
			gender: 'Male',
			date_of_birth: new Date('11/8/1994'),
			weightTracker: [
  			{
  				number: 2,
  				date: '10 May 2018',
  				kg: 64.5 
  			},
  			{
  				number: 1,
  				date: '2 May 2018',
  				kg: 65 
  			}
			],
			progressPhotos: [
				{
					comparasion: false,
					progress1: {
						photo: 'assets/img/progress/1.jpg',
						week: 1,
						weight: 50
					},
					progress2: {},
					caption: 'Keeping accountable by taking my first progress photo on Day 1 of my 8 week program!',
					tags: ['#2018goals', '#summer'],
					fb: false,
					date: '4 May, 2018'
				},
				{
					comparasion: false,
					progress1: {
						photo: 'assets/img/progress/2.jpg',
						week: 2,
						weight: 49.5
					},
					progress2: {},
					caption: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, accusamus.',
					tags: ['#fintess', '#instagirl'],
					fb: false,
					date: '10 May, 2018'
				},
				{
					comparasion: true,
					progress1: {
						photo: 'assets/img/progress/1.jpg',
						week: 2,
						weight: 49.5
					},
					progress2: {
						photo: 'assets/img/progress/2.jpg',
						week: 23,
						weight: 52
					},
					caption: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, accusamus.',
					tags: ['#fintess', '#instagirl'],
					fb: false,
					date: '10 May, 2018'
				}
			]
		}
	]
};