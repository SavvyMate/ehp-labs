import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'reverse'})
export class ReversePipe implements PipeTransform {
	transform(items: any[]) {
		return items.sort((a, b) => { return b.number - a.number });
	}
}