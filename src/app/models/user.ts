export class User {
	id: string;
	firstName: string;
	lastName: string;
	email: string;
	password: string;
	startingWeight: number;
	weightTracker: any[];
	progressPhotos: any[];
	logo: string;
	unitSystem: string;
	height: number;
	gender: string;
	date_of_birth: Date;
}