import { Component, OnInit, Input, AfterViewChecked } from '@angular/core';
import { WeightService } from '../../../services/weight.service';

@Component({
  selector: 'app-weight-chart',
  templateUrl: './weight-chart.component.html',
  styleUrls: ['./weight-chart.component.sass']
})
export class WeightChartComponent implements OnInit {

	@Input('dashboard') dashboard: boolean = false;

  constructor(public _weightService: WeightService) { }

  ngOnInit() {
    this._weightService.getProgress();
    if(!this._weightService.allowLoading) {
      setTimeout(() => {
        this._weightService.createChart();
        this._weightService.allowLoading = true;
      }, 1500);
    } else {
      	this._weightService.createChart();
    }
  }

}
