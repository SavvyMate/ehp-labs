import { Component, OnInit, HostListener } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.sass']
})
export class SettingsComponent implements OnInit {

	settings: User;

	dropDowns: boolean[] = [];

	changeEmail: boolean;

	crntPass: string;
	newPass: string;
	confPass: string;

	showCrntPass: string = 'hide';
	showNewPass: string = 'hide';
	showConfPass: string = 'hide';

	day: number;
	month: string;
	year: number;

	@HostListener('window:click') onClick() {
		for(let i = 0; i < this.dropDowns.length; i++) {
			this.dropDowns[i] = false;
		}
		this.changeEmail = false;
	}

  constructor(private _authService: AuthService) { }

  ngOnInit() {
  	this.settings = this._authService.currentUser;
  	this.day = this.settings.date_of_birth.getDate();
  	this.month = this.getMounts()[this.settings.date_of_birth.getMonth()];
  	this.year = this.settings.date_of_birth.getFullYear();
  }

  openDropdown(event, index: number) {
  	this.dropDowns[index] = true;
  	event.stopPropagation();
  }

  getDays() {
  	let days: number[] = [];
  	for(let i = 1; i <= 31; i++ ) {
  		days.push(i);
  	}
  	return days;
  }

  getMounts() {
  	let months: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  	return months;
  }

  getYears() {
  	let years: number[] = [];
  	for(let i = new Date().getFullYear(); i >= (new Date().getFullYear() - 75); i--) {
  		years.push(i);
  	}
  	return years;
  }

  confirmUpdates(settings: User) {
  	this.getMounts().forEach((month, index) => {
  		if(month.toLowerCase() === this.month.toLowerCase()) {
  			return this.month = index + 1 + '';
  		}
  	});

  	// Confirmation for passwords
  	// ...

  	// Update User Info
  	settings.date_of_birth = new Date(this.month + '/' + this.day + '/' + this.year);
  	this._authService.currentUser = settings;
  }

}
