import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

	correctEmail: boolean;
	email: string;
	emailNotFound: boolean;
	
	password: string;
	validPass: boolean = true;
	passVisibility: string = 'hide';

	remember: boolean;

	forgotPass: boolean;

  constructor(private router: Router, private _authService: AuthService) { }

  ngOnInit() {
  
  }


  checkEmail(email: string) {
  	if(email) {
	  	if(email === 'newuser@mail.com') {
	  		this.correctEmail = true;
	  	} else {
	  		this.emailNotFound = true;
	  	}
  	}
  }

  validatePassword(password: string) {
  	let regExp = /^\w*$/;
  	if(regExp.test(password)) {
  		this.validPass = true;
  	} else {
  		this.validPass = false;
  	}
  }

  checkPassword(password: string) {
  	if(password === '0000') {
  		this.router.navigateByUrl('/dashboard');
  		this._authService.logged = true;
  	}
  }

}
