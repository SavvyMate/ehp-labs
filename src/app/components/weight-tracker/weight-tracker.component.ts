import { Component, OnInit, HostListener, AfterContentChecked } from '@angular/core';
import { WeightService } from '../../services/weight.service';
import { AuthService } from '../../services/auth.service';
import { CurrentDateService } from '../../services/current-date.service';

@Component({
  selector: 'app-weight-tracker',
  templateUrl: './weight-tracker.component.html',
  styleUrls: ['./weight-tracker.component.sass']
})
export class WeightTrackerComponent implements OnInit {

	weight: boolean;

	@HostListener('window:resize') onResize() {
		this.equalHeight();
	}

  constructor(public _weightService: WeightService, public _authService: AuthService,
  	public _currentDateService: CurrentDateService) { }

  ngOnInit() {

  }

  equalHeight() {
  	if(window.innerWidth > 1200) {
	  	document.getElementById('weight-log').style.height = 
	  		document.getElementById('progress').clientHeight + 'px';
  	} else {
  		document.getElementById('weight-log').style.height = 'auto';
  	}
  }

  ngAfterContentChecked() {
  	this.equalHeight();
  }

}
