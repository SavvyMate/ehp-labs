import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CurrentDateService } from '../../../services/current-date.service';
import { AuthService } from '../../../services/auth.service';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-add-weight',
  templateUrl: './add-weight.component.html',
  styleUrls: ['./add-weight.component.sass'],
  animations: [
    trigger('slideInOut', [
      state('inactive', style({
        opacity: '0'
      })),
      state('active', style({
        opacity: '1'
      })),
      transition('inactive => active', animate('200ms ease-in')),
      transition('active => inactive', animate('200ms ease-out'))
    ])
  ]
})
export class AddWeightComponent implements OnInit {

	weight: string = '';
	validate: boolean = true;

  anim: string = 'inactive';

  @Output() close: EventEmitter<boolean> = new EventEmitter();
	@Output() update: EventEmitter<boolean> = new EventEmitter();

  constructor(public _currentDateService: CurrentDateService, public _authService: AuthService) { }

  ngOnInit() {
    setTimeout(() => {
      this.anim = 'active';
    }, 100);
  }

  addWeight(weight: string) {
  	if(Number(weight)) {
  		this._authService.currentUser.weightTracker.splice(0, 0,
  			{
  				number: this._authService.currentUser.weightTracker.length + 1,
  				date: this._currentDateService.today.toDateString(),
  				kg: Number(weight) 
  			}
  		);
  		this.update.emit(true);
      this.close.emit(false);
  	} else {
  		this.validate = false;
  	}
  }

  closeWeight() {
  	this.anim = 'inactive';
    setTimeout(() => {
      this.close.emit(false);
    }, 300);
  }

}
