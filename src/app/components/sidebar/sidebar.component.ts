import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {

	@Input('showSidebar') showSidebar: boolean;

  @Output() close: EventEmitter<boolean> = new EventEmitter();

  constructor(private _auth: AuthService, private router: Router) { }

  ngOnInit() {

  }

  logOut() {
  	this.router.navigateByUrl('/login');
  	this._auth.logged = false;
  }

}
