import { Component, OnInit, HostListener, AfterViewChecked } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { WeightService } from '../../services/weight.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  @HostListener('window:resize') onResize() {
		this.equalHeight();
	}

  constructor(public _authService: AuthService, private _weightService: WeightService) { }

  ngOnInit() {
    
  }

  equalHeight() {
  	if(window.innerWidth > 1200) {
	  	document.getElementById('weight-log').style.height = 
	  		document.getElementById('progress').clientHeight + 'px';
  	} else {
  		document.getElementById('weight-log').style.height = 'auto';
  	}
  }

  ngAfterContentChecked() {
  	this.equalHeight();
  }

}
