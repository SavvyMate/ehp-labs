import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.sass']
})
export class ProgressComponent implements OnInit {

	upload: boolean;
	create: boolean;
	share: boolean;

	week: boolean;
	week2: boolean;
	weight: boolean;
	weight2: boolean;

	progress: any;

	progress1: boolean;
	progress2: boolean;

	photosValid: boolean;

  constructor(public _authService: AuthService) { }

  ngOnInit() {
  	this.initProgress();
  }

  uploadProcces(comparasion: boolean) {
  	this.upload = true;
  	this.progress.comparasion = comparasion;
  }

  uploadImage(event, progress: string) {	
		if(event.target.value) {
			if(progress === '1') {
				this.progress.progress1.photo = 'assets/img/progress/1.jpg';
				this.progress1 = true; this.progress2 = false;
			} else if(progress === '2') {
				this.progress.progress2.photo = 'assets/img/progress/2.jpg';
				this.progress1 = false; this.progress2 = true;
			}

			if(!this.create && this.progress.comparasion && this.progress.progress1.photo && this.progress.progress2.photo) {
				this.create = true;
				this.progress1 = true; this.progress2 = false;
			} else if(!this.create && !this.progress.comparasion && this.progress.progress1.photo) {
				this.create = true;
				this.progress1 = true; this.progress2 = false;
			}
		}
	}

	initProgress() {
		this.progress = {
			comparasion: false,
			progress1: { photo: '', week: null, weight: null },
			progress2: { photo: '', week: null, weight: null },
			fb: false, caption: '', tags: [], date: ''
		};
	}

	shareProgress() {
		// Push progress
		if(this.photosValid && this.share) {
			// Validation
			if(!this.weight)
				this.progress.progress1.weight = null;
			if(!this.week)
				this.progress.progress1.week = null;
			if(!this.weight2)
				this.progress.progress2.weight = null;
			if(!this.week2)
				this.progress.progress2.week = null;
			// Add date
			this.progress.date = new Date();

			this.putTags(this.progress.caption);
			this._authService.currentUser.progressPhotos.push(this.progress);

			this.initProgress();
			this.upload = false;
			this.create = false;
			this.share = false;
			this.week = false;
			this.week2 = false;
			this.weight = false;
			this.weight2 = false;
		} else if(this.photosValid) {
			this.share = true;
		}

	}

	putTags(caption: string) {
		let text: string[] = [];
		text = caption.split(' ');
		this.progress.caption = '';
		text.forEach(tag => {
			if(tag[0] === '#') {
				this.progress.tags.push(tag);
			} else {
				this.progress.caption += tag + ' ';
			}
		});
	}

	disableCreate() {
		this.create = false;
		this.progress.progress1.photo = '';
		this.progress.progress2.photo = '';
	}

	photoValidation() {
		if(!this.progress.comparasion && this.progress.progress1.photo !== 'assets/img/spacer.png') {
			this.photosValid = true;
			return '1';
		} else if(this.progress.comparasion && this.progress.progress1.photo !== 'assets/img/spacer.png' && this.progress.progress2.photo !== 'assets/img/spacer.png') {
			this.photosValid = true;
			return '1';
		} else {
			this.photosValid = false;
			return '0.5';
		}
	}

}
