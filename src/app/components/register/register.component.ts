import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

	user: User;

	about: boolean = true;
	plan: boolean;
	pass: boolean;

	dropDowns: boolean[] = [];

	firstName: string = '';
	lastName: string = '';
	gender: string = '';

	day: number = 1;
	month: string = 'January';
	year: number = new Date().getFullYear();

	unitSystem: string = 'Metric (g, kg, cm, m)';

	height: number;
	weight: number;

	goal: string = 'Tone Up';
	goal_weight: number;
	dietary: string = '';
	experience: string = 'Beginner (0 - 6 months)';

	password: string;
	validPass: boolean = false;
	passVisibility: string = 'hide';

	@HostListener('window:click') onClick() {
		for(let i = 0; i < this.dropDowns.length; i++) {
			this.dropDowns[i] = false;
		}
	}

  constructor(private router: Router, private _authService: AuthService) { }

  ngOnInit() {

  }

  openDropdown(event, index: number) {
  	this.dropDowns[index] = !this.dropDowns[index];
  	event.stopPropagation();
  }

  getDays() {
  	let days: number[] = [];
  	for(let i = 1; i <= 31; i++ ) {
  		days.push(i);
  	}
  	return days;
  }

  getMounts() {
  	let months: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  	return months;
  }

  getYears() {
  	let years: number[] = [];
  	for(let i = new Date().getFullYear(); i >= (new Date().getFullYear() - 75); i--) {
  		years.push(i);
  	}
  	return years;
  }

  createPlan() {
  	// validate about info
  	this.plan = true;
  }

  createPass() {
  	// validate plan info
  	this.pass = true;
  }

  createUser() {
  	// data = user and then push to DB
  	this.router.navigateByUrl('/dashboard');
  	this._authService.logged = true;
  }

  validatePassword(password: string) {
  	let regExp = /^\w*$/;
  	if(regExp.test(password)) {
  		if(password.length >= 6) 
  			this.validPass = true;
  		else
  			this.validPass = false;
  	} else {
  		this.validPass = false;
  	}
  }

}
