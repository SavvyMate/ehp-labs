import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Chart } from 'chart.js';

@Injectable()
export class WeightService {

	result: string;
	progress: number;

	currentWeight: number;
	startWeight: number;

	chart: Chart = [];

  allowLoading: boolean;

  constructor(private _authService: AuthService) {
  	this.startWeight = _authService.currentUser.startingWeight;
  }

  createChart() {
  	this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: this.getWeeks(),
        datasets: [
          { data: this.getWeights(), borderColor: "#fff", fill: false }
        ]
      },
      options: {
        legend: { display: false },
        tooltips: { enabled: false },
        elements: {
          point: { backgroundColor: '#fff', radius: 7, hoverRadius: 6 }
        },
        layout: {
          padding: { left: 15, right: 40, top: 40, bottom: 15 }
        },
        scales: {
          xAxes: [{
            display: true,
            gridLines: { color: "transparent", drawBorder: false },
            ticks: { fontColor: '#fff' },
            scaleLabel: { display: true, fontSize: 14, labelString: 'Week', fontColor: '#fff' }
          }],
          yAxes: [{
            display: true,
            gridLines: { color: "rgba(255, 255, 255, .4)" },
            ticks: { fontColor: '#fff' },
            scaleLabel: { display: true, fontSize: 14, labelString: 'Weight', fontColor: '#fff' }
          }]
        }
      }
    });
  }

  updateChart() {
    this.chart.data.labels.push(this.getWeeks().length - 1);
    this.chart.data.datasets[0].data.push(this.currentWeight);
    this.chart.update();
  }

  getProgress() {
  	this.currentWeight = this._authService.currentUser.weightTracker[0].kg;
  	this.progress = this._authService.currentUser.startingWeight - this.currentWeight;
  	if(this.progress < 0) {
  		this.progress = Math.abs(this.progress);
  		this.result = 'positive';
  	} else {
  		this.result = 'negative';
  	}
  }

  getWeeks() {
    let weeks: number[] = [0];
    this._authService.currentUser.weightTracker.forEach(week => {
      weeks.splice(1, 0, week.number);
    });
    return weeks;
  }

  getWeights() {
    let weights: number[] = [this._authService.currentUser.startingWeight];
    this._authService.currentUser.weightTracker.forEach(weight => {
      weights.splice(1, 0, weight.kg);
    });
    return weights;
  }

}
