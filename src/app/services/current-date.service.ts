import { Injectable } from '@angular/core';

@Injectable()
export class CurrentDateService {

	today: Date;

  constructor() { 
  	this.today = new Date();
  }

}
