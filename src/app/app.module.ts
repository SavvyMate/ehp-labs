import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

// Services
import { AuthService } from './services/auth.service';
import { CurrentDateService } from './services/current-date.service';
import { WeightService } from './services/weight.service';

// Pipes
import { ReversePipe } from './pipes/reverse.pipe';

// Components
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';

// Pages
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MealPlanComponent } from './components/meal-plan/meal-plan.component';
import { WorkoutProgramComponent } from './components/workout-program/workout-program.component';
import { WeightTrackerComponent } from './components/weight-tracker/weight-tracker.component';
import { ProgressComponent } from './components/progress/progress.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SupplementPlanComponent } from './components/supplement-plan/supplement-plan.component';

// Additional
import { AddWeightComponent } from './components/weight-tracker/add-weight/add-weight.component';
import { WeightChartComponent } from './components/additional/weight-chart/weight-chart.component';

// Routes
import { appRoutes } from './app.router';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    DashboardComponent,
    MealPlanComponent,
    WorkoutProgramComponent,
    WeightTrackerComponent,
    ProgressComponent,
    SettingsComponent,
    AddWeightComponent,
    ReversePipe,
    WeightChartComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    SupplementPlanComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService, 
    CurrentDateService, 
    ReversePipe,
    WeightService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
