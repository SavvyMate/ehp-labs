import { Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MealPlanComponent } from './components/meal-plan/meal-plan.component';
import { WorkoutProgramComponent } from './components/workout-program/workout-program.component';
import { WeightTrackerComponent } from './components/weight-tracker/weight-tracker.component';
import { ProgressComponent } from './components/progress/progress.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SupplementPlanComponent } from './components/supplement-plan/supplement-plan.component';

export const appRoutes: Routes = [
	{ path: 'dashboard', component: DashboardComponent },
	{ path: 'meal-plan', component: MealPlanComponent },
	{ path: 'workout-program', component: WorkoutProgramComponent },
	{ path: 'weight-tracker', component: WeightTrackerComponent },
	{ path: 'progress', component: ProgressComponent },
	{ path: 'settings', component: SettingsComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'supplement-plan', component: SupplementPlanComponent },
	{ path: '', redirectTo: '/login', pathMatch: 'full' },
	{ path: '**', component: DashboardComponent }
];