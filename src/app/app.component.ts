import { Component, HostListener } from '@angular/core';
import { AuthService } from './services/auth.service';
import { WeightService } from './services/weight.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  
  sidebar: boolean;

  constructor(public _authService: AuthService, private _weightService: WeightService) {

  }

}
